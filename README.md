# Projet Generateur de groupe

Générateur de groupe Anthony, Gregorio, Florent, Jonathan.

Projet : « générateur de groupes »

Objectif
Créer une application qui regroupe les apprenants dans un nombre de groupes
donné.

Fonctionnalités
L'application contient la liste des apprenants. Elle demande le nombre de
groupes. Après avoir cliqué sur un bouton chaque apprenant est associé à un
groupe de façon aléatoire et équilibrée (le groupe le plus grand a au maximum
un membre de plus que le groupe le plus petit).
La liste des membres de chaque groupe est affichée.

Les différentes étapes

Créer les maquettes
Développer un algorithme
Coder l'application
Tester l'application
BONUS : Déployer l'application sur GitLab Pages


Organisation de travail
Chaque étape est développée en parallèle par 4 groupes. Quand tous les groupes
ont fini l'étape, les résultats sont comparés et un consensus est formé. Le
consensus sera la base pour l'étape suivante.

Technos

HTML5
CSS3
JavaScript
Git
GitLab


Livrables

La maquette
Le code source sur GitLab
BONUS : La page sur GitLab Pages


Durée
Un jour.